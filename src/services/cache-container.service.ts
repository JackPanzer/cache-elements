import { CacheElement } from "../classes/cache-element";

export class CacheContainerService {
    private invalidationTime: number;
    private storedData: Map<string, CacheElement>;

    constructor() {
        this.invalidationTime = 90;
        this.storedData = new Map<string, any>();
    }

    public invalidate() {
        this.storedData = new Map<string, any>();
    }

    public invalidateKey(key: string) {
        if(this.containsKey(key)) {
            this.storedData.delete(key);
        }
    }

    public containsKey(key: string) {
        return this.storedData.has(key);
    }

    private getInnerData(key: string) : Promise<any> {
        return new Promise((resolve, reject) => {
            try {
                const value = this.storedData.get(key);
                if(value.isExpired()) {
                    this.storedData.delete(key);
                    throw "Expired element";
                }
                resolve(value);
            }
            catch(exception) {
                reject(key);
            }
        });
    }

    public getData(key: string, action: Function): Promise<any> {
        return new Promise((resolve, reject) => {
            if(!this.containsKey(key)) {
                const actionResult = action();
                this.setData(key, actionResult);
                    resolve(actionResult);
            }
            else {
                this.getInnerData(key)
                    .then(value => {
                        resolve(value);
                    })
                    .catch(() => {
                        reject(key);
                    });
            }
        });
    }

    private setData(key: string, value: any): void {
        if(this.containsKey(key)) {
            this.storedData.delete(key);
        }
        this.storedData.set(key, new CacheElement(value, this.invalidationTime));
    }
}