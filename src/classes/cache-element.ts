export class CacheElement {
    public data: any;
    public expirationDate: Date;

    constructor(data: any, expiresIn: number) {
        this.data = data;

        const minutesInMiliseconds = expiresIn * 60 * 1000;
        const offset = Date.now() + minutesInMiliseconds;
        this.expirationDate = new Date(offset);
    }

    public isExpired(): boolean {
        return this.expirationDate < new Date();
    }
}