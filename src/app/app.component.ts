import { Component } from '@angular/core';
import { CacheContainerService } from '../services/cache-container.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent {
    lastValue: any;
    title = 'app';
    key = 0;
    wantedKey = '';
    wantedValue: any;

    constructor(private cache: CacheContainerService) {

    }

    addRandomKey(): void {
        var randomValue = Math.floor(Math.random() * 100000);

        this.getValueOf((this.key++).toString(), randomValue.toString());
    }

    getValueOf(key: string, value: string) {
        this.cache.getData(key, () => value)
            .then(value => {
                this.lastValue = value;
            });
    }

    retrieveValue(): void {
        if(this.cache.containsKey(this.wantedKey)) {
            this.cache.getData(this.wantedKey, () => -1)
                .then(value => {
                    console.log(value);
                    this.wantedValue = value.data;
                });
        }
        else {
            this.wantedValue = "No output";
        }
    }
}
